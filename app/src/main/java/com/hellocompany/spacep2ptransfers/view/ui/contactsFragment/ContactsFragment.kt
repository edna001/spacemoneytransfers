package com.hellocompany.spacep2ptransfers.view.ui.contactsFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.findNavController
import com.google.android.material.tabs.TabLayout
import com.hellocompany.spacep2ptransfers.R
import com.hellocompany.spacep2ptransfers.view.ui.contactsFragment.adapter.ContactsPagerAdapter
import kotlinx.android.synthetic.main.contacts_toolbar_view.*
import kotlinx.android.synthetic.main.fragment_contacts.*

class ContactsFragment : Fragment(R.layout.fragment_contacts) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back_to_transfers.setOnClickListener {
            fragmentManager!!.popBackStack()
        }

        contacts_view_pager.adapter = ContactsPagerAdapter(childFragmentManager)
        contacts_view_pager.addOnPageChangeListener(object : TabLayout.TabLayoutOnPageChangeListener(contacts_tab_layout){})
        contacts_tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {}
            override fun onTabUnselected(p0: TabLayout.Tab?) {}
            override fun onTabSelected(tab: TabLayout.Tab?) {
                contacts_view_pager.currentItem = tab!!.position
            }
        })
    }
}
