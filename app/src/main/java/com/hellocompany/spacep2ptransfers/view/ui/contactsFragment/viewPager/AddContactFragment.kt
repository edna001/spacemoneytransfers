package com.hellocompany.spacep2ptransfers.view.ui.contactsFragment.viewPager

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentUris
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager

import com.hellocompany.spacep2ptransfers.R
import com.hellocompany.spacep2ptransfers.service.model.Contact
import com.hellocompany.spacep2ptransfers.view.ui.contactsFragment.adapter.ContactsListAdapter
import kotlinx.android.synthetic.main.fragment_add_contact.*

class AddContactFragment : Fragment(R.layout.fragment_add_contact) {
    private val RPC: Int = 1
    private var contactsListAdapter: ContactsListAdapter? = null
    private var listOfContacts: ArrayList<Contact>? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showContacts()
        contactsListAdapter = ContactsListAdapter(listOfContacts!!, activity!!.supportFragmentManager)
        recycler_view_contacts.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = contactsListAdapter
        }
        //search contacts
        search_view.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(filterText: CharSequence?, p1: Int, p2: Int, p3: Int) {
                contactsListAdapter!!.filter.filter(filterText)
            }
        })
    }

    private fun showContacts() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), RPC)
        } else {
            listOfContacts = getContacts()
        }
    }
    @SuppressLint("Recycle")
    private fun getContacts(): ArrayList<Contact> {
        val listOfContacts: ArrayList<Contact> = arrayListOf()
        val projection: Array<String> = arrayOf(
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.PHOTO_ID,
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID)
        val cursor = context!!.contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            projection, null, null, null)
        if (cursor != null){
            while (cursor.moveToNext()){
                val name = cursor.getString(0)
                val number = cursor.getString(1)
                val photoId = cursor.getLong(2)
                val contactId = cursor.getLong(3)
                var bitmap: Bitmap? = null
                if (photoId > 0){
                    val uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId)
                    val inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context!!.contentResolver, uri)
                    bitmap = BitmapFactory.decodeStream(inputStream)
                }
                val listOfNumbers: ArrayList<String> = arrayListOf()
                listOfNumbers.add(number)
                listOfContacts.add(
                    Contact(name, number, listOfNumbers, bitmap, null)
                )
                listOfNumbers.clear()
            }
        }
        cursor!!.close()
        //if one name has several numbers
        for (contact in listOfContacts) {
            val name = contact.contactName
            for (currentContact in listOfContacts) {
                if (name == currentContact.contactName
                    && !currentContact.listOfNumbers!!.contains(contact.cMobileNumber)) {
                    currentContact.listOfNumbers!!.add(contact.cMobileNumber!!)
                    contact.hasSeveralNumbers = true
                    if (currentContact.listOfNumbers!!.size == 1)
                        contact.hasSeveralNumbers = false
                }
            }
        }
        return listOfContacts
    }

//    //function for hide keyboard
//    private fun hideKeyboard(view: View){
//        val inputManager: InputMethodManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        inputManager.hideSoftInputFromWindow(view.windowToken, 0)
//    }
//
//    hide keyboard
//        search_view.onFocusChangeListener = View.OnFocusChangeListener { myView, hasFocus ->
//            if (!hasFocus)
//                hideKeyboard(myView)
//        }
}
