package com.hellocompany.spacep2ptransfers.view.ui.transfersFragment.viewPager

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.hellocompany.spacep2ptransfers.R
import kotlinx.android.synthetic.main.fragment_template.*

class TemplateFragment : Fragment(R.layout.fragment_template) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mSettings = this.activity!!.getSharedPreferences("mypreferences", Context.MODE_PRIVATE)
        if (mSettings.contains("contactName")) {
            template_get.visibility = View.VISIBLE
            template_get_name.text = mSettings.getString("contactName", "")
            template_get_number.text = mSettings.getString("mobileNumber", "")
        }
    }
}
