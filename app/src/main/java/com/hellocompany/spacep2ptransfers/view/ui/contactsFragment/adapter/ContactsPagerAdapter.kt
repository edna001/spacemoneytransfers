package com.hellocompany.spacep2ptransfers.view.ui.contactsFragment.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.hellocompany.spacep2ptransfers.view.ui.contactsFragment.viewPager.AddAccountFragment
import com.hellocompany.spacep2ptransfers.view.ui.contactsFragment.viewPager.AddContactFragment
import com.hellocompany.spacep2ptransfers.view.ui.contactsFragment.viewPager.AddLinkFragment
import java.lang.IllegalArgumentException

@Suppress("DEPRECATION")
class ContactsPagerAdapter(fManager: FragmentManager): FragmentPagerAdapter(fManager) {

    override fun getItem(position: Int): Fragment {
        return when(position) {
            //if position = 0 => return AddContactFragment
            0 -> AddContactFragment()
            //if position = 1 => return AddContactFragment
            1 -> AddAccountFragment()
            //if position = 2 => return AddContactFragment
            2 -> AddLinkFragment()
            else -> throw IllegalArgumentException("Position not valid")
        }
    }

    override fun getCount(): Int {
        return 3
    }
}