package com.hellocompany.spacep2ptransfers.view.ui.contactsFragment.viewPager

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.hellocompany.spacep2ptransfers.R

class AddLinkFragment : Fragment(R.layout.fragment_add_link)