package com.hellocompany.spacep2ptransfers.view.ui.transfersFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.navGraphViewModels
import com.google.android.material.tabs.TabLayout
import com.hellocompany.spacep2ptransfers.R
import com.hellocompany.spacep2ptransfers.view.ui.contactsFragment.ContactsFragment
import com.hellocompany.spacep2ptransfers.view.ui.transfersFragment.adapter.TransfersPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_transfers.*
import kotlinx.android.synthetic.main.transfer_toolbar_view.*

class TransfersFragment : Fragment(R.layout.fragment_transfers) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        transfer_bottom_bar.setOnClickListener {
            fragmentAdd(ContactsFragment())
        }

        transfer_view_pager.adapter = TransfersPagerAdapter(childFragmentManager)
        transfer_view_pager.addOnPageChangeListener(object : TabLayout.TabLayoutOnPageChangeListener(transfer_tab_layout){})
        transfer_tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {}
            override fun onTabUnselected(p0: TabLayout.Tab?) {}
            override fun onTabSelected(tab: TabLayout.Tab?) {
                transfer_view_pager.currentItem = tab!!.position
            }
        })
    }

    private fun fragmentAdd(frag: Fragment){
        fragmentManager!!.beginTransaction().setCustomAnimations(
            R.anim.slide_in_left_animation,
            R.anim.slide_in_rigth_animation,
            R.anim.slide_in_left_animation,
            R.anim.slide_in_rigth_animation)
            .addToBackStack(null)
            .replace(R.id.nav_host_fragment, frag).commit()
    }

}
