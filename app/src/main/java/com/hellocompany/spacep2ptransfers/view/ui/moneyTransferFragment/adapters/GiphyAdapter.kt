package com.hellocompany.spacep2ptransfers.view.ui.moneyTransferFragment.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.hellocompany.spacep2ptransfers.R
import com.hellocompany.spacep2ptransfers.service.model.GiphyGif
import kotlinx.android.synthetic.main.giphy_item.view.*

class GiphyAdapter(private val giphyList: List<GiphyGif>,
                   var gifCallBack: ((gif: String) -> Unit)? = null) :
    RecyclerView.Adapter<GiphyAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.giphy_item, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return giphyList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val giphyGif = giphyList[position].images.fixed_width.url
        Glide.with(holder.giphyView.context)
            .asGif().load(giphyGif)
            .listener(object : RequestListener<GifDrawable>{
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<GifDrawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    holder.progress.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(
                    resource: GifDrawable?,
                    model: Any?,
                    target: Target<GifDrawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    holder.progress.visibility = View.GONE
                    return false
                }

            })
            .into(holder.giphyView)
        holder.itemView.setOnClickListener {
            gifCallBack?.invoke(giphyGif)
        }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val giphyView = view.giphy_item
        val progress = view.giphy_progress
    }

}