package com.hellocompany.spacep2ptransfers.view.ui.creditCardFragment

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.EditText
import android.widget.Toast
import cards.pay.paycardsrecognizer.sdk.Card
import cards.pay.paycardsrecognizer.sdk.ScanCardIntent

import com.hellocompany.spacep2ptransfers.R
import com.hellocompany.spacep2ptransfers.view.ui.moneyTransferFragment.MoneyTransferFragment
import kotlinx.android.synthetic.main.fragment_credit_card.*

class CreditCardFragment : Fragment(R.layout.fragment_credit_card) {

    private lateinit var moneyTransferFragment: MoneyTransferFragment
    val REQUEST_CODE_SCAN_CARD: Int = 1
    var cardData: String? = null
    val bundle: Bundle = Bundle()
    val TAG = "Scan Card"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args: Bundle = arguments!!
        val contactIcon: Bitmap? = args.getParcelable("contactIcon")
        val contactName = args.getString("contactName")
        val cMobileNumber = args.getString("mobileNumber")
        val chooseMoney = args.getString("chooseMoney")

        scan_card.setOnClickListener { scanCard(card_id_edit) }

        credit_card_bottom_bar.setOnClickListener {
            if (card_id_edit.text.length == 16) {
                moneyTransferFragment =
                    MoneyTransferFragment()
                bundle.putString("chooseMoney", chooseMoney)
                bundle.putString("contactName", contactName)
                bundle.putString("mobileNumber", cMobileNumber)
                bundle.putParcelable("contactIcon", contactIcon)
                moneyTransferFragment.arguments = bundle
                fragmentReplace(moneyTransferFragment)
            } else
                Toast.makeText(context, "ბარათის ნომერი უნდა იყოს 16 ციფრი", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SCAN_CARD) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val card: Card = data!!.getParcelableExtra(ScanCardIntent.RESULT_PAYCARDS_CARD)!!
                    cardData = card.cardNumberRedacted + " " + card.cardHolderName + " " + card.expirationDate
                    Log.i(TAG, "Card info " + cardData)
                }
                Activity.RESULT_CANCELED -> Log.i(TAG, "Scan canceled")
                else -> Log.i("Scan Card", "Scan failed")
            }
        }
    }

    private fun scanCard(cardId: EditText) {
        val intent = ScanCardIntent.Builder(context).build()
        startActivityForResult(intent, REQUEST_CODE_SCAN_CARD)
        if (cardData != null)
            cardId.setText(cardData)
    }

    private fun fragmentReplace(frag: Fragment){
        fragmentManager!!.beginTransaction().setCustomAnimations(
            R.anim.slide_in_left_animation,
            R.anim.slide_in_rigth_animation,
            R.anim.slide_in_left_animation,
            R.anim.slide_in_rigth_animation)
            .addToBackStack(null)
            .replace(R.id.nav_host_fragment, frag).commit()
    }
}
