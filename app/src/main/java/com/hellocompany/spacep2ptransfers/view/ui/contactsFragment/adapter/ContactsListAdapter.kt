package com.hellocompany.spacep2ptransfers.view.ui.contactsFragment.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Filter
import android.widget.Filterable
import android.widget.RadioButton
import androidx.core.view.marginTop
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.hellocompany.spacep2ptransfers.R
import com.hellocompany.spacep2ptransfers.service.model.Contact
import com.hellocompany.spacep2ptransfers.view.ui.moneyChossingFragment.MoneyChoosingFragment
import kotlinx.android.synthetic.main.bottom_dialog_view.view.*
import kotlinx.android.synthetic.main.contact_item.view.*

class ContactsListAdapter(private val arrayList: List<Contact>,
                          private val fManager: FragmentManager):
    RecyclerView.Adapter<ContactsListAdapter.ViewHolder>(), Filterable {

    private var filteredList: List<Contact> = ArrayList<Contact>(arrayList)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.contact_item,
                parent,
                false
            )
        )
    }
    override fun getItemCount(): Int {
        return filteredList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = filteredList[position]
        //set animations
        holder.contactIcon.animation = AnimationUtils.loadAnimation(holder.itemView.context, R.anim.scale_animation)
        holder.hasSpaceAccount.animation = AnimationUtils.loadAnimation(holder.itemView.context, R.anim.scale_animation)
        holder.contactName.animation = AnimationUtils.loadAnimation(holder.itemView.context, R.anim.translate_animation)
        holder.phoneNumber.animation = AnimationUtils.loadAnimation(holder.itemView.context, R.anim.translate_2_animation)
        //set info
        holder.contactName.text = item.contactName
        when (item.hasSeveralNumbers) {
            true -> {
                var numbersList = ""
                for (numbers in item.listOfNumbers!!)
                    numbersList += "$numbers; "
                holder.phoneNumber.text = numbersList
            }
            false -> holder.phoneNumber.text = item.cMobileNumber
        }
        //if not exist image, that set default image
        if (item.contactPhoto == null)
            holder.contactIcon.setImageResource(R.drawable.ic_money)
        else
            holder.contactIcon.setImageBitmap(item.contactPhoto)

        holder.itemView.setOnClickListener {
            if (item.hasSeveralNumbers!! ) {
                val bottomDialog = BottomSheetDialog(holder.itemView.context, R.style.BottomSheetDialog)
                val dialogView = LayoutInflater.from(holder.itemView.context)
                    .inflate(R.layout.bottom_dialog_view, null)
                dialogView.contact_name_d.text = filteredList[position].contactName
                for (numbers in item.listOfNumbers!!) {
                    val radioButton = RadioButton(holder.itemView.context)
                    radioButton.text = numbers
                    dialogView.numbers_group_d.addView(radioButton)
                }
                bottomDialog.setContentView(dialogView)
                dialogView.numbers_group_d.setOnCheckedChangeListener{ group, id ->
                    val rb: RadioButton = group.findViewById(id)
                    val bundle = Bundle()
                    bundle.putString("contactName", item.contactName)
                    bundle.putString("mobileNumber", rb.text.toString())
                    bundle.putParcelable("contactIcon", item.contactPhoto)
                    val moneyChoosingFragment =
                        MoneyChoosingFragment()
                    moneyChoosingFragment.arguments = bundle
                    fragmentReplace(moneyChoosingFragment)
                    bottomDialog.dismiss()
                }
                bottomDialog.show()
            } else {
                val bundle = Bundle()
                bundle.putString("contactName", item.contactName)
                bundle.putString("mobileNumber", item.cMobileNumber)
                bundle.putParcelable("contactIcon", item.contactPhoto)
                val moneyChoosingFragment =
                    MoneyChoosingFragment()
                moneyChoosingFragment.arguments = bundle
                fragmentReplace(moneyChoosingFragment)
            }
        }
    }
    //search filter
    override fun getFilter(): Filter {
        return contactsFilter
    }
    private val contactsFilter = object : Filter(){
        override fun performFiltering(text: CharSequence?): FilterResults {
            val searchText = text.toString()

            if (searchText.isEmpty()) {
                filteredList = arrayList
            }
            else {
                val filterList = ArrayList<Contact>()
                for (item in arrayList){
                    if(item.contactName!!.toLowerCase().contains(searchText))
                        filterList.add(item)
                    else if (item.cMobileNumber!!.contains(searchText))
                        filterList.add(item)
                }
                ifContactsListIsNull(searchText, filterList)
                filteredList = filterList
            }
            val results = FilterResults()
            results.values = filteredList
            return results
        }

        override fun publishResults(text: CharSequence?, results: FilterResults?) {
            filteredList = results!!.values as List<Contact>
            notifyDataSetChanged()
        }
    }
    private fun ifContactsListIsNull(text: String, list: ArrayList<Contact>){
        if (list.isEmpty()){
            val newContact = Contact(
                contactName = "ახალი მიმღები",
                cMobileNumber = text,
                contactPhoto = null,
                listOfNumbers = null,
                hasSeveralNumbers = null
            )
            list.add(newContact)
        }
    }
    private fun fragmentReplace(frag: Fragment){
        fManager.beginTransaction().setCustomAnimations(
            R.anim.slide_in_left_animation,
            R.anim.slide_in_rigth_animation,
            R.anim.slide_in_left_animation,
            R.anim.slide_in_rigth_animation)
            .addToBackStack(null)
            .replace(R.id.nav_host_fragment, frag).commit()
    }
    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val contactName = view.contact_name!!
        val phoneNumber = view.phone_number!!
        val contactIcon = view.contact_icon!!
        val hasSpaceAccount = view.has_space_account!!
    }
}