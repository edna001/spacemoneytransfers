package com.hellocompany.spacep2ptransfers.view.ui.transfersFragment.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.hellocompany.spacep2ptransfers.view.ui.transfersFragment.viewPager.PayFragment
import com.hellocompany.spacep2ptransfers.view.ui.transfersFragment.viewPager.TemplateFragment

class TransfersPagerAdapter(fManager: FragmentManager): FragmentStatePagerAdapter(fManager) {

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> TemplateFragment()
            1 -> PayFragment()
            else -> throw IllegalArgumentException("Position not valid")
        }
    }

    override fun getCount(): Int {
        return 2
    }

}