package com.hellocompany.spacep2ptransfers.view.ui.moneyTransferFragment

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.hellocompany.spacep2ptransfers.R
import com.hellocompany.spacep2ptransfers.service.model.GiphyGif
import com.hellocompany.spacep2ptransfers.service.model.GiphyResponse
import com.hellocompany.spacep2ptransfers.service.repository.GiphyService
import com.hellocompany.spacep2ptransfers.view.ui.moneyTransferFragment.adapters.GiphyAdapter
import kotlinx.android.synthetic.main.fragment_money_transfer.*
import kotlinx.android.synthetic.main.giphy_view.view.*
import kotlinx.android.synthetic.main.message_view.*
import kotlinx.android.synthetic.main.template_item.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoneyTransferFragment : Fragment(R.layout.fragment_money_transfer) {

  private val limit = 50
  private val PREFERENCES = "mypreferences"
  private val apiKey = "SttVxkYS3qWRnZKeSldHL7qL9wu6PZgq"
  lateinit var giphyAdapter: GiphyAdapter
  lateinit var bottomDialog: BottomSheetDialog
  lateinit var messageIcon: ImageView
  lateinit var messageRemoveButton: ImageButton
  lateinit var gifButton: Button
  lateinit var gifCard: CardView

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    val args: Bundle = arguments!!
    val contactIcon: Bitmap? = args.getParcelable("contactIcon")
    val contactName = args.getString("contactName")
    val cMobileNumber = args.getString("mobileNumber")
    val chooseMoney = args.getString("chooseMoney")
    gifButton = view.findViewById(R.id.gif_btn)
    messageIcon = view.findViewById(R.id.message_gif)
    gifCard = view.findViewById(R.id.message_gif_card)
    messageRemoveButton = view.findViewById(R.id.message_gif_remove)
    bottomDialog = BottomSheetDialog(context!!, R.style.BottomSheetDialog)

    money_transfer_contact_name.text = contactName
    money_transfer_phone_number.text = cMobileNumber
    money_transfer_text.text = chooseMoney
    when(contactIcon) {
      //when contact icon is empty -> set default contact icon
      null -> money_transfer_contact_icon.setImageResource(R.drawable.ic_money)
      //when contact icon isn't empty
      else -> money_transfer_contact_icon.setImageBitmap(contactIcon)
    }

    messageRemoveButton.setOnClickListener {
      gifCard.visibility = View.INVISIBLE
      messageIcon.visibility = View.INVISIBLE
      messageRemoveButton.visibility = View.INVISIBLE
      gifButton.visibility = View.VISIBLE
    }

    //gif button select
    gifButton.setOnClickListener { gifPopUp() }
    save_template_button.setOnClickListener {
      val textVis = save_template_text.visibility
      val viewVis = save_template.visibility
      when(textVis) {
        View.GONE -> {
          save_template_text.visibility = View.VISIBLE
          save_template_button.setImageResource(R.drawable.ic_star)
        }
        View.VISIBLE -> {
          save_template_text.visibility = View.GONE
          save_template_button.setImageResource(R.drawable.ic_star_blue)
        }
      }
      when(viewVis) {
        View.VISIBLE -> save_template.visibility = View.GONE
        View.GONE -> {
          save_template.visibility = View.VISIBLE
          template_icon.setImageBitmap(contactIcon)
        }
      }
    }
    money_transfer_bottom_bar.setOnClickListener {
      if(save_template.visibility == View.VISIBLE) {
        val mSettings = this.activity!!.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE)
        val mEditor = mSettings.edit()
        mEditor.putString("contactName", template_name_input.text.toString())
        mEditor.putString("mobileNumber", cMobileNumber)
        mEditor.apply()
      }
    }
  }

  //gif pop up function
  private fun gifPopUp() {
    //creating layout for bottom dialog
    val dialogView = LayoutInflater.from(context).inflate(R.layout.giphy_view, null)
    //load trending giphies
    loadGiphies(dialogView, bottomDialog)
    //search giphies
    dialogView.giphy_search.addTextChangedListener(object : TextWatcher{
      override fun afterTextChanged(text: Editable?) {
        when(text.toString()) {
          //when search text is empty -> get trending giphies
          "" -> loadGiphies(dialogView, bottomDialog)
          //when search text isn't empty -> get searched giphies
          else -> searchGiphies(dialogView, text.toString(), bottomDialog)
        }
      }
      override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
      override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    })
    //setting layout on bottom dialog
    bottomDialog.setContentView(dialogView)
    //show bottom dialog
    bottomDialog.show()
  }
  //getting popular giphies function
  private fun loadGiphies(view: View, dialog: BottomSheetDialog) {
    GiphyService.create().getPopularGiphies(apiKey, limit).enqueue(object : Callback<GiphyResponse>{
      override fun onFailure(call: Call<GiphyResponse>, t: Throwable) {}
      override fun onResponse(call: Call<GiphyResponse>, response: Response<GiphyResponse>) {
        giphyAdapter =
          GiphyAdapter(
            response.body()!!.data, gifCallBack = { gif ->
              bottomDialog.dismiss()
              Glide.with(context!!)
                .asGif()
                .load(gif)
                .into(messageIcon)
              gifCard.visibility = View.VISIBLE
              gifButton.visibility = View.INVISIBLE
              messageIcon.visibility = View.VISIBLE
              messageRemoveButton.visibility = View.VISIBLE
            })
        view.giphy_rec_view.apply {
          layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
          adapter = giphyAdapter
        }
      }
    })
  }
  //searching giphies function
  private fun searchGiphies(view: View, text: String, dialog: BottomSheetDialog) {
    GiphyService.create().getSearchedGiphies(text, apiKey, limit).
      enqueue(object : Callback<GiphyResponse>{
        override fun onFailure(call: Call<GiphyResponse>, t: Throwable) {}
        override fun onResponse(call: Call<GiphyResponse>, response: Response<GiphyResponse>) {
          if (response.body()!!.data.isEmpty()) {
            view.giphy_rec_view.visibility = View.INVISIBLE
            view.giphy_not_found.visibility = View.VISIBLE
          } else {
            if (view.giphy_not_found.visibility == View.VISIBLE)
              view.giphy_not_found.visibility = View.INVISIBLE
            if (view.giphy_rec_view.visibility == View.INVISIBLE)
              view.giphy_rec_view.visibility = View.VISIBLE
            giphyAdapter =
              GiphyAdapter(
                response.body()!!.data, gifCallBack = { gif ->
                  bottomDialog.dismiss()
                  Glide.with(context!!)
                    .asGif()
                    .load(gif)
                    .into(messageIcon)
                  gifCard.visibility = View.VISIBLE
                  gifButton.visibility = View.INVISIBLE
                  messageIcon.visibility = View.VISIBLE
                  messageRemoveButton.visibility = View.VISIBLE
                })
            view.giphy_rec_view.apply {
              layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
              adapter = giphyAdapter
            }
          }
        }
      })
  }
}
