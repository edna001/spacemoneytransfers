package com.hellocompany.spacep2ptransfers.view.ui.moneyChossingFragment

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

import com.hellocompany.spacep2ptransfers.R
import com.hellocompany.spacep2ptransfers.view.ui.creditCardFragment.CreditCardFragment
import kotlinx.android.synthetic.main.fragment_money_choosing.*
import kotlinx.android.synthetic.main.keyboard_view.*

class MoneyChoosingFragment : Fragment(R.layout.fragment_money_choosing) {

    lateinit var creditCardFragment: CreditCardFragment
    val bundle: Bundle = Bundle()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args: Bundle = arguments!!
        val contactIcon: Bitmap? = args.getParcelable("contactIcon")
        val contactName = args.getString("contactName")
        val cMobileNumber = args.getString("mobileNumber")

        money_contact_name.text = contactName
        money_phone_number.text = cMobileNumber
        if (contactIcon == null)
            money_contact_icon.setImageResource(R.drawable.ic_money)
        else
            money_contact_icon.setImageBitmap(contactIcon)

        keyboardButtons()
        money_choosing_bottom_bar.setOnClickListener {
            if (money_text.text.toString().toDouble() >= 1.0) {
                creditCardFragment =
                    CreditCardFragment()
                bundle.putString("chooseMoney", money_text.text.toString())
                bundle.putString("contactName", contactName)
                bundle.putString("mobileNumber", cMobileNumber)
                bundle.putParcelable("contactIcon", contactIcon)
                creditCardFragment.arguments = bundle
                fragmentReplace(creditCardFragment)
            } else
                Toast.makeText(context, "გთხოვთ მიუთითეთ 0.99 ლარზე მეტი თანხა", Toast.LENGTH_SHORT).show()

        }
    }

    private fun fragmentReplace(frag: Fragment){
        fragmentManager!!.beginTransaction().setCustomAnimations(
            R.anim.slide_in_left_animation,
            R.anim.slide_in_rigth_animation,
            R.anim.slide_in_left_animation,
            R.anim.slide_in_rigth_animation)
            .addToBackStack(null)
            .replace(R.id.nav_host_fragment, frag).commit()
    }

    private fun keyboardButtons() {
        k_one.setOnClickListener { keyboardFunctional(money_text, k_one) }
        k_two.setOnClickListener { keyboardFunctional(money_text, k_two) }
        k_three.setOnClickListener { keyboardFunctional(money_text, k_three) }
        k_four.setOnClickListener { keyboardFunctional(money_text, k_four) }
        k_five.setOnClickListener { keyboardFunctional(money_text, k_five) }
        k_six.setOnClickListener { keyboardFunctional(money_text, k_six) }
        k_seven.setOnClickListener { keyboardFunctional(money_text, k_seven) }
        k_eight.setOnClickListener { keyboardFunctional(money_text, k_eight) }
        k_nine.setOnClickListener { keyboardFunctional(money_text, k_nine) }
        k_zero.setOnClickListener { keyboardFunctional(money_text, k_zero) }
        k_dot.setOnClickListener { keyboardDot(money_text, k_dot) }
        k_delete.setOnClickListener { keyboardRemove(money_text) }
    }

    @SuppressLint("SetTextI18n")
    private fun keyboardFunctional(moneyText: TextView, buttonText: Button) {
        if (moneyText.text == "0" || moneyText.text == " 0")
            moneyText.text = ""
        moneyText.text = "${moneyText.text}${buttonText.text}"
    }

    @SuppressLint("SetTextI18n")
    private fun keyboardDot(moneyText: TextView, buttonText: Button) {
        if (moneyText.text.contains("."))
            return
        else
            moneyText.text = " ${moneyText.text}${buttonText.text}"

    }

    private fun keyboardRemove(moneyText: TextView) {
        val length = moneyText.text.length
        if (moneyText.text.isEmpty() || moneyText.text == " ")
            moneyText.text = "0"
        else if (moneyText.text.isNotEmpty()) {
            moneyText.text = moneyText.text.removeRange(length - 1, length)
            if (moneyText.text.isEmpty() || moneyText.text == " ")
                moneyText.text = "0"
        }
    }
}
