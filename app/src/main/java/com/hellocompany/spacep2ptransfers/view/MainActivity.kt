package com.hellocompany.spacep2ptransfers.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import com.hellocompany.spacep2ptransfers.R
import com.hellocompany.spacep2ptransfers.view.ui.transfersFragment.TransfersFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
        transfer_minus.setOnClickListener {
            fragmentReplace(TransfersFragment())
        }
    }

    private fun fragmentReplace(frag: Fragment){
        supportFragmentManager.beginTransaction().setCustomAnimations(
            R.anim.slide_in_left_animation,
            R.anim.slide_in_rigth_animation,
            R.anim.slide_in_left_animation,
            R.anim.slide_in_rigth_animation)
            .addToBackStack(null)
            .replace(R.id.nav_host_fragment, frag).commit()
    }

}

