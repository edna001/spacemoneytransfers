package com.hellocompany.spacep2ptransfers.service.repository

import com.hellocompany.spacep2ptransfers.service.model.GiphyResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface GiphyService {

    companion object {
        fun create(): GiphyService {
            val retrofit = Retrofit.Builder().baseUrl("https://api.giphy.com/v1/gifs/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(GiphyService::class.java)
        }
    }

    @GET("trending?")
    fun getPopularGiphies(
        @Query("api_key") key: String,
        @Query("limit") limit: Int
    ): Call<GiphyResponse>

    @GET("search?")
    fun getSearchedGiphies(
        @Query("q") search: String,
        @Query("api_key") key: String,
        @Query("limit") limit: Int
    ): Call<GiphyResponse>
}