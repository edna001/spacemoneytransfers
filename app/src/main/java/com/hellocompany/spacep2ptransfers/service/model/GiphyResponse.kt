package com.hellocompany.spacep2ptransfers.service.model

data class GiphyResponse(
    var data: List<GiphyGif>
)