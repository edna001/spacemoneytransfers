package com.hellocompany.spacep2ptransfers.service.model

import android.graphics.Bitmap

data class Contact(
//    val contactId: String?,
    val contactName: String?,
    var cMobileNumber: String?,
    var listOfNumbers: ArrayList<String>?,
    val contactPhoto: Bitmap?,
    var hasSeveralNumbers: Boolean?
)