package com.hellocompany.spacep2ptransfers.service.model

import com.google.gson.annotations.SerializedName

data class GiphyGif(
    val id: String,
    val type: String,
    val images: GiphyImage
)